#pragma once

#include <QMainWindow>
#include <QGraphicsScene>
#include <QLabel>

#include <vector>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

enum class STATE
{
    ALGORITHM_SELECTION,
    INPUT,
    CALCULATION_DONE
};

enum class ALGORITHM
{
    NONE,
    TRIANGULATION,
    CONVEX_HULL,
    MEC
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);

public slots:
    //
    // Custom slot for adding points onto the scene using left-click
    //
    void slotAddPointToScene(const QPointF& point);

    //
    // Custom slot for closing polygon in triangulation algorithm using right-click
    //
    void slotClosePolygon();

    void slotTriangulationButton();

    void slotConvexHullButton();

    void slotSmallestCircleButton();

    void slotAddPointsButton();

    void slotGeneratePointsButton();

    void slotSubstractPointsButton();

    void slotCalculateButton();

    void slotResetButton();

private:
    // UI and general widgets
    Ui::MainWindow* ui;
    QGraphicsScene* scene;

    // Geometric objects
    std::vector<QGraphicsEllipseItem*> points;
    std::vector<QGraphicsLineItem*>    lines;
    std::vector<QGraphicsEllipseItem*> circles;

    // State of the program and status bar variables
    STATE State;
    QLabel* state_label;

    ALGORITHM Algorithm;
    QLabel* algorithm_label;

    QLabel* points_count;

    int points_to_generate_count;

    // Drawing
    QColor yellow;
    QColor magenta;
    QPen magenta_pen;

    // Functions
    void triangulation();
    void convexHull();
    void MEC();

    void init();
    void enterInputState();
    void enableAlgorithmButtons(const bool enable);
    void enableGenerationAndCalculationButtons(const bool enable);

    void addNewPoint(const int x, const int y);
};




