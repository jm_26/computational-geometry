#pragma once

#include <QGraphicsEllipseItem>

#include <vector>
#include <stack>

struct Point
{
    int x, y;

    Point() {}
    Point(int x, int y) : x(x), y(y) {}
};

struct Circle
{
    double x, y;
    double radius;

    Circle(double x, double y, double r) : x(x), y(y), radius(r) {}
    Circle(Point p, double r) : x(p.x), y(p.y), radius(r) {}
};

struct Triangle
{
    Point a, b, c;

    Triangle(Point a, Point b, Point c) : a(a), b(b), c(c) {}
};

class Algorithms
{
public:

    static int randomInt(const int min, const int max);

    static double distance(const Point& p1, const Point& p2);

    static std::vector<Triangle> earClipping(const std::vector<QGraphicsEllipseItem*>& ellipses);

    static std::stack<Point> GrahamScan(const std::vector<QGraphicsEllipseItem*>& ellipses);

    static Circle RitterBoundingSphere(const std::vector<QGraphicsEllipseItem*>& ellipses);

private:

    static std::vector<Point> QGraphicsEllipsesToPoints(const std::vector<QGraphicsEllipseItem*>& ellipses);

    static bool isPointInTriangle(const Triangle& t, const Point& p);

    static bool isEar(const std::vector<Point>& vertices, const int i);

    static int pointsOrientation(const Point& p1, const Point& p2, const Point& p3);
};

