#include "algorithms.h"

#include <random>
#include <cmath>
#include <algorithm>

int Algorithms::randomInt(const int min, const int max)
{
    if (min == max) return min;

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distr(min, max);

    return distr(gen);
}

std::vector<Triangle> Algorithms::earClipping(const std::vector<QGraphicsEllipseItem*>& ellipses)
{
    // Representation of polygon as an ordered set of vertices
    std::vector<Point> vertices = QGraphicsEllipsesToPoints(ellipses);

    std::vector<Triangle> triangles;

    // While at least 3 vertices, go through all adjacent triples of vertices and
    // if they form an ear, remove the middle vertex and continue on smaller polygon
    while (vertices.size() >= 3)
    {
        for (size_t i = 0; i < vertices.size(); ++i)
        {
            if(isEar(vertices, i))
            {
                int prev = (i + vertices.size() - 1) % vertices.size();
                int next = (i + 1) % vertices.size();
                triangles.push_back({vertices[prev], vertices[i], vertices[next]});
                vertices.erase(vertices.begin() + i);
                break;
            }
        }
    }

    return triangles;
}

std::stack<Point> Algorithms::GrahamScan(const std::vector<QGraphicsEllipseItem*>& ellipses)
{
    std::vector<Point> points = QGraphicsEllipsesToPoints(ellipses);

    // Create stack which will contain points in convex hull
    std::stack<Point> convex_hull;

    // Find Point with minimal y value and push it into the stack
    Point lowest = points[0];
    int min_index = 0;

    for (size_t i = 1; i < points.size(); ++i)
    {
        if (points[i].y < lowest.y)
        {
            lowest = points[i];
            min_index = i;
        }
    }

    // Push the lowest point into convex hull and remove it from points
    convex_hull.push(lowest);
    points.erase(points.begin() + min_index);

    // Calculate angle between the lowest point and other points
    std::vector<std::pair<Point, double>> point_angle_pairs;

    for (Point& point : points)
    {
        double y_diff = point.y - lowest.y;
        double x_diff = point.x - lowest.x;

        double angle = atan2(y_diff, x_diff);

        point_angle_pairs.emplace_back(point, angle);
    }

    // Sort the pairs according to their angle in ascending order
    std::sort(point_angle_pairs.begin(), point_angle_pairs.end(), [](const auto& lhs, const auto& rhs) {
        return lhs.second < rhs.second;
    });

    // Push point with the smallest angle into the stack and remove it
    convex_hull.push(point_angle_pairs[0].first);
    point_angle_pairs.erase(point_angle_pairs.begin() + 0);

    // Main loop - find consecutive triples of points which have
    // a counterclockwise orientation and push them into the stack
    for (size_t i = 0; i < point_angle_pairs.size(); ++i)
    {
        while (convex_hull.size() > 1)
        {
            Point p1 = convex_hull.top();
            convex_hull.pop();

            Point p0 = convex_hull.top();

            if (pointsOrientation(p0, p1, point_angle_pairs[i].first) != -1)
            {
                convex_hull.push(p1);
                break;
            }
        }

        convex_hull.push(point_angle_pairs[i].first);
    }

    return convex_hull;
}

Circle Algorithms::RitterBoundingSphere(const std::vector<QGraphicsEllipseItem*>& ellipses)
{
    std::vector<Point> points = QGraphicsEllipsesToPoints(ellipses);

    // Choose random point x from P
    Point x = points[rand() % points.size()];

    // Find y from P with largest distance from x
    Point y(0, 0);
    double max_dist = 0;
    for (Point& p : points)
    {
        if (distance(p, x) > max_dist)
        {
            max_dist = distance(p, x);
            y = p;
        }
    }

    // Find z from P with largest distance from y
    Point z(0, 0);
    max_dist = 0;
    for (Point& p : points)
    {
        if (distance(p, y) > max_dist)
        {
            max_dist = distance(p, y);
            z = p;
        }
    }

    // Create circle so that y and z lies on it
    Point centerYZ((y.x + z.x) * 0.5, (y.y + z.y) * 0.5);
    double radius = distance(y, z) * 0.5;

    // Iterate over all points and make sure they are all in the circle
    for (const Point& p : points)
    {
        if (distance(centerYZ, p) > radius)
        {
            double d = distance(centerYZ, p);
            radius = (radius + d) * 0.5;
            Point diff(p.x - centerYZ.x, p.y - centerYZ.y);
            double t = radius / d;
            centerYZ = Point(p.x - t * diff.x, p.y - t * diff.y);
        }
    }

    return Circle(centerYZ, radius);
}

double Algorithms::distance(const Point& p1, const Point& p2)
{
    return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
}

std::vector<Point> Algorithms::QGraphicsEllipsesToPoints(const std::vector<QGraphicsEllipseItem*>& ellipses)
{
    std::vector<Point> points;

    for (auto& ellipse : ellipses)
        points.push_back(Point(ellipse->x(), ellipse->y()));

    return points;
}

bool Algorithms::isPointInTriangle(const Triangle& tr, const Point& p)
{
    Point a = tr.a;
    Point b = tr.b;
    Point c = tr.c;

    double area = 0.5 * (-b.y * c.x + a.y * (-b.x + c.x) + a.x * (b.y - c.y) + b.x * c.y);

    double s = 1 / (2 * area) * (a.y * c.x - a.x * c.y + (c.y - a.y) * p.x + (a.x - c.x) * p.y);
    double t = 1 / (2 * area) * (a.x * b.y - a.y * b.x + (a.y - b.y) * p.x + (b.x - a.x) * p.y);

    return s > 0 && t > 0 && 1-s-t > 0;
}

bool Algorithms::isEar(const std::vector<Point>& vertices, const int i)
{
    // Checks, whether vertices i-1, i, i+1 create an ear
    const int n = vertices.size();
    int prev = (i + n - 1) % n;
    int next = (i + 1) % n;
    for(int j = 0; j < n; ++j)
    {
        if (pointsOrientation(vertices[prev], vertices[i], vertices[next]) == -1)
            return false;
        if(j != prev && j != i && j != next && isPointInTriangle(Triangle(vertices[prev], vertices[i], vertices[next]), vertices[j]))
            return false;
    }
    return true;
}

int Algorithms::pointsOrientation(const Point& p1, const Point& p2, const Point& p3)
{
    double area = (p2.x - p1.x) * (p3.y - p1.y) - (p2.y - p1.y) * (p3.x - p1.x);

    if (area < 0) return -1; // clockwise

    if (area > 0) return 1; // counterclockwise

    return 0; // collinear
}
