#include "mainwindow.h"
#include "customgraphicsview.h"
#include "ui_mainwindow.h"
#include "algorithms.h"

#include <QGraphicsEllipseItem>
#include <QGraphicsLineItem>
#include <QString>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setFixedSize(1034, 750);
    setWindowTitle("Computational Geometry");

    scene = new QGraphicsScene(this);
    scene->setSceneRect(0, 0, 790, 640);

    ui->canvas->setScene(scene);
    ui->canvas->scale(1, -1);

    QList<QPushButton *> buttons = this->findChildren<QPushButton *>();

    for(QPushButton* button : buttons)
    {
        button->setStyleSheet(R"(
            QPushButton {
                background-color: #404040;
                border: none;
                color: white;
                padding: 5px;
                text-align: center;
                text-decoration: none;
                font-size: 16px;
                margin: 4px 2px;
                border-radius: 8px;
            }

            QPushButton:hover {
                background-color: #505050;
            }

            QPushButton:pressed {
                background-color: #303030;
            }

            QPushButton:disabled {
                background-color: #202020;
                color: #808080;
            }
            )");
    }

    ui->triangulation_button->setToolTip("Using Ear clipping algorihtm triangulates given simple polygon. Close the polygon with right-click.");
    ui->convex_hull_button->setToolTip("Using Graham scan algorithm computes convex hull of given set of points.");
    ui->smallest_circle_button->setToolTip("Using Ritter's bounding algorithm computes smallest enclosing circle for given set of points.");
    ui->generate_points_button->setToolTip("Uniformly generate given number of points. Prefer not using for smallest circle algorithm.");
    ui->calculate_button->setToolTip("Calculates the result after inputting points.");
    ui->reset_button->setToolTip("Reset into initial state.");

    connect(ui->canvas,  &CustomGraphicsView::pointCreated, this, &MainWindow::slotAddPointToScene);
    connect(ui->canvas,  &CustomGraphicsView::closePolygon, this, &MainWindow::slotClosePolygon);
    connect(ui->triangulation_button,   SIGNAL (clicked()), this, SLOT (slotTriangulationButton()));
    connect(ui->convex_hull_button,     SIGNAL (clicked()), this, SLOT (slotConvexHullButton()));
    connect(ui->smallest_circle_button, SIGNAL (clicked()), this, SLOT (slotSmallestCircleButton()));
    connect(ui->add_button,             SIGNAL (clicked()), this, SLOT (slotAddPointsButton()));
    connect(ui->generate_points_button, SIGNAL (clicked()), this, SLOT (slotGeneratePointsButton()));
    connect(ui->substract_button,       SIGNAL (clicked()), this, SLOT (slotSubstractPointsButton()));
    connect(ui->calculate_button,       SIGNAL (clicked()), this, SLOT (slotCalculateButton()));
    connect(ui->reset_button,           SIGNAL (clicked()), this, SLOT (slotResetButton()));

    state_label     = new QLabel();
    algorithm_label = new QLabel();
    points_count    = new QLabel();

    yellow = QColor(0xF5, 0xF5, 0x5A);
    magenta = QColor(0xFD, 0x37, 0xFD);
    magenta_pen.setColor(magenta);

    init();
}

void MainWindow::slotAddPointToScene(const QPointF& point)
{
    if (State == STATE::INPUT)
    {
        addNewPoint(point.x(), point.y());

        // If we are in triangulation algorithm and have at least two points,
        // draw lines showing present part of the polygon
        if (Algorithm == ALGORITHM::TRIANGULATION && points.size() > 1)
        {
            for (size_t i = 0; i < points.size() - 1; ++i)
            {
                Point a(points[i]->x(), points[i]->y());
                Point b(points[i+1]->x(), points[i+1]->y());
                QGraphicsLineItem* line = scene->addLine(a.x, a.y, b.x, b.y, magenta_pen);
                lines.push_back(line);
            }
        }
    }
    else
    {
        ui->statusbar->showMessage("You have to be in the Input state.");
    }
}

void MainWindow::slotClosePolygon()
{
    if (Algorithm == ALGORITHM::TRIANGULATION)
    {
        // Add the last line to close polygon and automatically call triangulation function
        if (points.size() > 2)
        {
            Point first(points[0]->x(), points[0]->y());
            Point last(points[points.size() - 1]->x(), points[points.size() - 1]->y());

            QGraphicsLineItem* line = scene->addLine(last.x, last.y, first.x, first.y, magenta_pen);
            lines.push_back(line);
        }

        triangulation();
    }
}

void MainWindow::slotTriangulationButton()
{
    Algorithm = ALGORITHM::TRIANGULATION;
    enterInputState();
}

void MainWindow::slotConvexHullButton()
{
    Algorithm = ALGORITHM::CONVEX_HULL;
    enterInputState();
}

void MainWindow::slotSmallestCircleButton()
{
    Algorithm = ALGORITHM::MEC;
    enterInputState();
}

void MainWindow::slotAddPointsButton()
{
    points_to_generate_count += 5;
    ui->generate_points_button->setText("Generate " + QString::number(points_to_generate_count) + " points.");
}

void MainWindow::slotGeneratePointsButton()
{
    // TODO disable for triangulation
    if (Algorithm == ALGORITHM::CONVEX_HULL || Algorithm == ALGORITHM::MEC)
    {
        for (int i = 0; i < points_to_generate_count; ++i)
        {
            int x = Algorithms::randomInt(0, 790);
            int y = Algorithms::randomInt(0, 640);

            addNewPoint(x, y);
        }
    }
}

void MainWindow::slotSubstractPointsButton()
{
    if (points_to_generate_count > 5)
    {
        points_to_generate_count -= 5;
        ui->generate_points_button->setText("Generate " + QString::number(points_to_generate_count) + " points.");
    }
}

void MainWindow::slotCalculateButton()
{
    state_label->setText("Calculation done.");

    switch (Algorithm)
    {
    case ALGORITHM::NONE:
        // It should not reach here
        break;
    case ALGORITHM::TRIANGULATION:
        triangulation();
        break;
    case ALGORITHM::CONVEX_HULL:
        convexHull();
        break;
    case ALGORITHM::MEC:
        MEC();
        break;
    }
}

void MainWindow::slotResetButton()
{
    // Delete geometric objects and reset state

    for (auto& item : points)
    {
        scene->removeItem(item);
        delete item;
    }
    points.clear();

    for (auto& line : lines)
    {
        scene->removeItem(line);
        delete line;
    }
    lines.clear();

    for (auto& circle : circles)
    {
        scene->removeItem(circle);
        delete circle;
    }
    circles.clear();

    init();
}

void MainWindow::triangulation()
{
    if (points.size() < 3)
    {
        ui->statusbar->showMessage("You must have at least 3 lines.", 4000);
    }
    else
    {
        std::vector<Triangle> triangles = Algorithms::earClipping(points);

        // Draw the triangles
        for (Triangle& t : triangles)
        {
            Point a = t.a;
            Point b = t.b;
            Point c = t.c;

            QGraphicsLineItem* line1 = scene->addLine(a.x, a.y, b.x, b.y, magenta_pen);
            QGraphicsLineItem* line2 = scene->addLine(b.x, b.y, c.x, c.y, magenta_pen);
            QGraphicsLineItem* line3 = scene->addLine(c.x, c.y, a.x, a.y, magenta_pen);

            lines.push_back(line1);
            lines.push_back(line2);
            lines.push_back(line3);
        }

        enableGenerationAndCalculationButtons(false);
        ui->reset_button->setEnabled(true);
        State = STATE::CALCULATION_DONE;
    }

    state_label->setText("Calculation done.");
}

void MainWindow::convexHull()
{
    if (points.size() < 2)
    {
        ui->statusbar->showMessage("You must have at least 2 points.", 4000);
    }
    else
    {
        std::stack<Point> convex_hull = Algorithms::GrahamScan(points);

        // Draw the convex hull
        std::vector<Point> hull;
        while (!convex_hull.empty())
        {
            hull.push_back(convex_hull.top());
            convex_hull.pop();
        }

        for (size_t i = 0; i < hull.size(); ++i)
        {
            Point a = hull[i % hull.size()];
            Point b = hull[(i+1) % hull.size()];

            QGraphicsLineItem* line = scene->addLine(a.x, a.y, b.x, b.y, magenta_pen);
            lines.push_back(line);
        }

        enableGenerationAndCalculationButtons(false);
        ui->reset_button->setEnabled(true);
        State = STATE::CALCULATION_DONE;
    }
}

void MainWindow::MEC()
{
    if (points.size() < 2)
    {
        ui->statusbar->showMessage("You have to be in the IDLE state and have at least 2 points.", 4000);
    }
    else
    {
        Circle R = Algorithms::RitterBoundingSphere(points);

        // Draw the MEC
        QGraphicsEllipseItem* circle = scene->addEllipse(R.x - R.radius, R.y - R.radius, R.radius * 2, R.radius * 2, magenta_pen);
        circles.push_back(circle);

        enableGenerationAndCalculationButtons(false);
        ui->reset_button->setEnabled(true);
        State = STATE::CALCULATION_DONE;
    }
}

void MainWindow::init()
{
    State = STATE::ALGORITHM_SELECTION;
    Algorithm = ALGORITHM::NONE;

    state_label->setText("Algorithm selection state.");
    algorithm_label->setText("Algorithm chosen: NONE");
    points_count->setText("Number of points: " + QString::number(points.size()));

    ui->statusbar->addPermanentWidget(state_label);
    ui->statusbar->addPermanentWidget(algorithm_label);
    ui->statusbar->addPermanentWidget(points_count);

    enableAlgorithmButtons(true);
    enableGenerationAndCalculationButtons(false);
    ui->reset_button->setDisabled(true);

    points_to_generate_count = 10;
    ui->generate_points_button->setText("Generate " + QString::number(points_to_generate_count) + " points");
}

void MainWindow::enterInputState()
{
    State = STATE::INPUT;

    enableAlgorithmButtons(false);
    if (Algorithm != ALGORITHM::TRIANGULATION)
        enableGenerationAndCalculationButtons(true);

    state_label->setText("Input state.");
    QString chosen_algorithm;

    if (Algorithm == ALGORITHM::TRIANGULATION) {
        chosen_algorithm = "Triangulation";
    } else if (Algorithm == ALGORITHM::CONVEX_HULL){
        chosen_algorithm = "Convex hull";
    } else if (Algorithm == ALGORITHM::MEC) {
        chosen_algorithm = "Smalles circle";
    }
    algorithm_label->setText("Algorithm chosen: " + chosen_algorithm);
}

void MainWindow::enableAlgorithmButtons(const bool enable)
{
    ui->triangulation_button->setEnabled(enable);
    ui->convex_hull_button->setEnabled(enable);
    ui->smallest_circle_button->setEnabled(enable);
}

void MainWindow::enableGenerationAndCalculationButtons(const bool enable)
{
    ui->add_button->setEnabled(enable);
    ui->generate_points_button->setEnabled(enable);
    ui->substract_button->setEnabled(enable);
    ui->calculate_button->setEnabled(enable);
}

void MainWindow::addNewPoint(const int x, const int y)
{
    QGraphicsEllipseItem* point = new QGraphicsEllipseItem(-2, -2, 4, 4);
    point->setPos(x, y);
    point->setBrush(QBrush(yellow));

    scene->addItem(point);

    points.push_back(point);
    points_count->setText("Number of points: " + QString::number(points.size()));
}



