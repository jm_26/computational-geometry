#include "customgraphicsview.h"

void CustomGraphicsView::mousePressEvent(QMouseEvent* event)
{
    if (event->button() == Qt::LeftButton)
    {
        QPointF scenePoint = mapToScene(event->pos());

        emit pointCreated(scenePoint);
    }

    if (event->button() == Qt::RightButton)
    {
        emit closePolygon();
    }

    QGraphicsView::mousePressEvent(event);
}
