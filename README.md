# Computational Geometry Project

## Table of Content
- [Project Description](#project-description)
- [Installation](#installation)
- [Usage](#usage)
- [Algorithms implemented](#algorithms-implemented)
- [Author](#author)
- [Sources](#sources)

## Project Description
This project aims to implement few of the **computational geometry algorithms**, in particular **triangulation** of simple polygon and finding **convex hull** and **minimal enclosing circle (MEC)** of given set of points. The algorithms are implemented as a simple GUI application using the **C++** programming language and the **Qt** toolkit for handling graphical widgets and components.

## Installation
1. **Prerequisities** 

Make sure you have Qt toolkit installed. It can be downloaded from [Qt Download](https://www.qt.io/download).

2. **Clone the repository**  

Clone the repository to your local machine using `git clone https://gitlab.com/jm_26/computational-geometry/`.

3. **Open the project** 

Open the project in the Qt Creator by opening the *.pro* file. 

4. **Build the project** 

In the Qt Creator you can build the project e.g. with the <kbd>Ctrl+B</kbd> shortcut. 

5. **Run the project** 

In the Qt Creator you can run the project e.g. with the <kbd>Ctrl+R</kbd> shortcut.

## Usage
After starting the application you will see main window which contains several buttons for controlling the application, scene on the right which will display user input and result of the algorithms and status bar at the bottom which shows information about state of the program. The usage of the program may be divided into few steps: 

1. **Choosing algorithm**

User chooses algorithm by clicking on one of the three algorithm buttons. 

2. **Inputting the points**

User can input points into the scene using left-click of the mouse or generate them randomly. Since triangulation expects the input as a simple polygon the points are gradually connected with lines and the last point to the first point is connected with right-click of the mouse. 

3. **Calculation** 

After inputting the points click on the calculate button for calculating and showing the final result. Be aware that for the triangulation algorithm, the calculation button is disabled and the calculation is performed right after the right-click.

4. **Reset**

After the calculation finishes, the program can be reset into initial state using the reset button.

---

The status bar located at the bottom shows information about the current state of program, chosen algorithm and number of points inputted. Occasionally, temporary message on the left of the status bar 
pops out to guide the user e.g. if he tries to input points before selecting algorithm. Also, the area of the polygon in the triangulation section is showed here.

## Algorithms implemented
In this section we will discuss the problems and algorithms in greater details. 

- **Triangulation**

For Triangulation of given simple polynom is performed using the **Ear clipping algorithm**. This is one of the simples algorithms for performing triangulation which runs in $O(n^2)$ where $n$ is number of vertices in the polygon. 

It is based on the fact that any simple polygon (polygon with no holes which is not intersecting itself) with at least four vertices can be triangulated. The algorithm gradually goes through all adjacent triples of vertices looking for an ear. An ear is triplet of adjacent points for which two facts hold: first, the middle points with the adjacent ones forms a convex vertex and second, no other point in the polygon lies in the triangle formed by those three points. After the ear is found the middle point can be removed from the polygon, triangle consisting of those three points is saved and program continues again on smaller polygon. 

Note that this is one of the simpler algorithms for triangulation. Faster and more general algorithms which run in $O(n \  log \ n)$ and are applicable even for polygons with holes exist. 

- **Convex hull** 

Finding a convex hull of given set of points is perford using the Graham scan algorithm. This algorithm runs in $O(n \ log \ n)$ where $n$ is number of points inputted. 

The algorithm first finds the lowest of all points (point with the smallest y-coordinate). Then it goes through all other points and calculates polar angle between the lowest point and the other points and sort the other points in ascending order based on the angle. Then it places the lowest point and point with smallest angle on stack. After that the main loop begins in which it places next point on the stack and checks whether the top three points form a convex angle. If yes, continue adding more points and if not, remove points from the stack until the condition is met. 

- **Smallest enclosing circle** 

Finding smallest enclosing circle is performed by the Ritter's bounding sphere algorithm. This is a heuristic algorithm which for any given constant dimension $d$ runs in $O(n)$, where $n$ is number of points inputted. 

The algorithm first randomly choses point $x$ from set of all points $P$ and then it finds another point $y$ furthest from $x$. After that it find another point $z$ furthest from $y$ and sets up circle with middle point with the centre being mid point on the line $xy$ and radius begin half the distance from $x$ to $y$. After setting up the initial circle it goes through all he points and checks whether they are contain in the cicle. If they are not, enlarge the circle so that they are contained. 

## Author
Jan Miesbauer,
FNSPE CTU, branch Mathematical Informatics,
Academic year 2022-23.

## Sources
- **Triangulation**
[Clipping ear algorithm](https://www.geometrictools.com/Documentation/TriangulationByEarClipping.pdf)

- **Convex hull** 
[Graham scan algorithm](https://en.wikipedia.org/wiki/Graham_scan)


- **Smallest enclosing circle**
[Bounding sphere algorithm](https://en.wikipedia.org/wiki/Bounding_sphere)


